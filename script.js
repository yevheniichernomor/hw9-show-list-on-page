/* Відповіді:
1. Для створення тегу використовується метод document.createElement(tag).

2. В першому параметрі функції elem.insertAdjacentHTML вказується, куди буде вставлено рядок HTML відносно elem.
beforebegin - перед elem, afterbegin – в elem, на початку, beforeend – в elem, в кінці, afterend - після elem.

3. Для видалення елементу використовується метод node.remove().
 */

function createList(parent, ...listItems) {
	let array = [...listItems];

	if (parent == ''){
		parent = document.body;

		for(let element of array){
			let listItem = document.createElement('li');
			listItem.innerHTML = element;
			parent.append(listItem);
		}
	} 
	else {
		let list = document.createElement('ul');
		list.innerHTML = parent;
		document.body.append(list);

		for(let element of array){
			let listItem = document.createElement('li');
			listItem.innerHTML = element;
			list.append(listItem);
		}
	}
}

let res = createList('List', '1', 2, 3, 'Lol', 'auto', 'PC');

let sec = 3;

function timeSec() {
    if (sec < 1) {
        document.body.innerHTML = '';
    }
    else {
        document.getElementById('timer').innerHTML = sec;
        setTimeout(timeSec, 1000);
        sec--;
    }
}
timeSec();